DIR_PLUG = src/plug/
DIR_BS   = src/base_station/
DIR_RMOTE= src/remote/
DIR_INC  = include/

INC_FILE = $(notdir $(wildcard $(DIR_INC)*h))

all : 
	ln -f -s include/*.h -r -t src/plug/
	ln -f -s include/*.h -r -t src/base_station/
	ln -f -s include/*.h -r -t src/remote/

clean:
	rm -f $(addprefix $(DIR_PLUG), $(INC_FILE))
	rm -f $(addprefix $(DIR_BS), $(INC_FILE))
	rm -f $(addprefix $(DIR_RMOTE), $(INC_FILE))
