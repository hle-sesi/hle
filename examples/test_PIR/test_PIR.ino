/*
 * PIR sensor tester
 */
int Pin_PIR = 13;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status


void setup() {
  pinMode(Pin_PIR, INPUT);     
  Serial.begin(9600);
}
void loop(){
  val = digitalRead(Pin_PIR);  
  
  if (val == HIGH) {            // check if motion ( Vout(PIR)~5V )
      if (pirState == LOW) {
        // we have just turned on
        Serial.println("Motion detected!");
        // We only want to print on the output change, not state
        pirState = HIGH;
      }
  } 
  else   // not motion ( Vout(PIR)=0V )
  {
      if (pirState == HIGH){
        // we have just turned of
        Serial.println("Motion ended!");
        // We only want to print on the output change, not state
        pirState = LOW;
      }
  }
}
