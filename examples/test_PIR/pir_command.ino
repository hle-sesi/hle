
int vout_PIR = 13;               // PIR sensor         
int supply_PIR = 12;                // power of the PIR 
int cut_power = 1;               // 1: power off
int vout_PIR_last = 1;
int vout_PIR_new = 0;

//*********************************************//
void setup() {
  Serial.begin(115200);
  
  pinMode(vout_PIR, INPUT);
        
  pinMode(supply_PIR, OUTPUT);
  digitalWrite(supply_PIR,HIGH);  //switch on the PIR
}
void loop()
{
    static uint32_t lastMillis =0 ;
    vout_PIR_new = digitalRead(vout_PIR);
    
    //Serial.println("vout_PIR_new: " + String(vout_PIR_new) );
    //Serial.println("vout_PIR_last: " + String(vout_PIR_last) );
    //delay(1000);
    
    if( ((vout_PIR_new)== 1) && ((vout_PIR_last)== 0) && (cut_power== 0) ) //rising
    {
        Serial.println("---------------");
        Serial.println("Motion detected ! ");
        Serial.println("---------------");

        vout_PIR_last = vout_PIR_new;
        //Serial.println("vout_PIR_last: " + String(vout_PIR_last) );
        
        cut_power = 1;
        lastMillis = millis();

        //------cut power------------
        digitalWrite(supply_PIR,LOW);
        delay(5000);
        //Serial.println("ici");
        digitalWrite(supply_PIR,HIGH);
        //Serial.println("ici2");
    }

    if ( (cut_power== 1) &&(millis() - lastMillis > 10000 ) ) //10s vout: 1->0
    {
        //Serial.println("10s");
        if (vout_PIR_new== 0)
        {
            vout_PIR_last = vout_PIR_new ;
            //Serial.println("vout_PIR_last: " + String(vout_PIR_last) );
            cut_power=0;
        }
    }  
   
}
