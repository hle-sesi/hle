#include <AESLib.h>

#include <SPI.h>
#include <LoRa.h>
#include<Arduino.h>
#include <U8g2lib.h>   // https://github.com/olikraus/U8g2_Arduino

// GPIO5  -- SX1278's SCK
// GPIO19 -- SX1278's MISO
// GPIO27 -- SX1278's MOSI
// GPIO18 -- SX1278's CS
// GPIO14 -- SX1278's RESET
// GPIO26 -- SX1278's IRQ(Interrupt Request)

#define SS      18
#define RST     14
#define DI0     26
#define BAND    433E6  //915E6 -- 这里的模式选择中，检查一下是否可在中国实用915这个频段


// I2C OLED Display works with SSD1306 driver
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16

U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0, /* clock=*/ OLED_SCL, /* data=*/ OLED_SDA, /* reset=*/ OLED_RST); // Full framebuffer, SW I2C

////////////////////////////////////////////////////////////
#define SIZE          32
#define NB_PLUGS      32 //
#define TYPE_SB_PLUG  0x1
#define SENDER        0x80 //SB
#define PING          0
#define PLUG_ON       0x1
#define PLUG_OFF      0x2

byte msg_id = 0;
byte buff[SIZE];

//////////////////////////////////////////////////////////////////////////////////////////////

void setup() {

      pinMode(25,OUTPUT); //Send success, LED will bright 1 second
  
      Serial.begin(115200);
      while (!Serial); //If just the the basic function, must connect to a computer
      
      SPI.begin(5,19,27,18);
      LoRa.setPins(SS,RST,DI0);
    //  Serial.println("LoRa Sender");
    
      Display.begin();
      Display.enableUTF8Print();    // enable UTF8 support for the Arduino print() function
      Display.setFont(u8g2_font_ncenB10_tr);
    
      if (!LoRa.begin(BAND)) {
        Serial.println("Starting LoRa failed!");
        while (1);
      }
      Serial.println("LoRa Initial OK!");
      
      //--------------CRC-------------//
      LoRa.crc();      
      //-------------Initialization------------//       
      for(int i=0;i<NB_PLUGS;i++)
      {
        buff[i]=0;
        //Serial.println("buff : " + String(buff[i],HEX));
      }

      task_plug_on(0);
      delay(3000);
      task_ping(0);
      delay(3000);
      task_plug_off(0);
}

void loop()
{
  task_listen();
}

//----------------Send a packet---------------------
void send_packet(byte type,byte sender,byte msg_id,byte* buff)
{
  Serial.println("sender : " + String(sender,HEX));
  Serial.println("msg_ID :" + String(msg_id,HEX));
   
  LoRa.beginPacket();
  LoRa.write(type);
  LoRa.write(sender);
  LoRa.write(msg_id);
  LoRa.write(buff,SIZE);  
  LoRa.endPacket();                     // finish packet and send it
}
//------------------buffer-------------------------
// plug >= 0
void new_buffer(byte* buff,int plug, int cmd)
{
  Serial.println("cmd: " + String(cmd));
  //Serial.println("plug: " + String(plug/2));
  if( ((plug)%2) == 1 )
      buff[plug/2] = buff[plug/2] & 0x0F ; //initialize to 0 the command(4bit) of the plug
  else
      buff[plug/2] = buff[plug/2] & 0xF0 ; //initialize to 0 the command(4bit) of the plug

  buff[plug/2] |= (cmd) << ( 4*((plug)%2) );
  Serial.println("buff_new : " + String(buff[plug/2], HEX));
}

void task_ping(int plug_number)
{
    Serial.println("--------------------------------------------------------------------------------");
    Serial.print("Sending packet: ");
    Serial.println(msg_id);
    ////////////////////////////////////
    byte type = TYPE_SB_PLUG; //SB
    byte sender = SENDER;
    //buff[plug_number]= PING; //plug[plug_number] : mode PING
    new_buffer(buff,0,PING);    
    
    //-------------------Send packet--------------------------------
    send_packet(type,sender,msg_id,buff);
    msg_id++;    
}

void task_plug_on(int plug_number)
{
    Serial.println("--------------------------------------------------------------------------------");
    Serial.print("Sending packet: ");
    Serial.println(msg_id);
    ////////////////////////////////////
    byte type = TYPE_SB_PLUG; //SB
    byte sender = SENDER;
    //buff[plug_number]= PLUG_ON; //plug[plug_number] : mode ON
    new_buffer(buff,0,PLUG_ON);
    
    //-------------------Send packet--------------------------------
    send_packet(type,sender,msg_id,buff);
    msg_id++;            
}

void task_plug_off(int plug_number)
{
    Serial.println("--------------------------------------------------------------------------------");
    Serial.print("Sending packet: ");
    Serial.println(msg_id);
    ////////////////////////////////////
    byte type = TYPE_SB_PLUG; //SB
    byte sender = SENDER;
    //buff[plug_number]= PLUG_OFF; //plug[plug_number] : mode OFF
    new_buffer(buff,0,PLUG_OFF);
    
    //-------------------Send packet--------------------------------
    send_packet(type,sender,msg_id,buff);
    msg_id++;    
}

void task_listen()
{
      //Serial.println("**ecoute**");
      //-------------------ACK----------------------------------------
      // try to parse packet
      int packetSize = LoRa.parsePacket();
      if (packetSize) {
          // received a packet
          Serial.println("--------------------------------------------------");
          Serial.println("[ACK] Received packet ..... ");
      
          // read packet
          while (LoRa.available()) {
            byte type_0 = LoRa.read();
            byte sender_0 = LoRa.read();
            byte msg_id_0 = LoRa.read();
            byte cmd_0 = LoRa.read();
            byte ack_0 = LoRa.read();
            Serial.println("type0 : " + String(type_0, HEX));
            Serial.println("sender0 : " + String(sender_0, HEX));
            Serial.println("msg_ID : " + String(msg_id_0, HEX));
            Serial.println("cmd_0 : " + String(cmd_0, HEX));
            Serial.println("ack : "  + String(ack_0,HEX));
          }         
      }//if (packetSize)  
}
