#include <Stepper.h>

#include <AESLib.h>

#include <SPI.h>
#include <LoRa.h>

#include <string.h>


#include <U8g2lib.h>   // https://github.com/olikraus/U8g2_Arduino

#include "Arduino.h"

// GPIO5  -- SX1278's SCK
// GPIO19 -- SX1278's MISO
// GPIO27 -- SX1278's MOSI
// GPIO18 -- SX1278's CS
// GPIO14 -- SX1278's RESET
// GPIO26 -- SX1278's IRQ(Interrupt Request)

#define SS      18
#define RST     14
#define DI0     26
#define BAND    433E6

// I2C OLED Display works with SSD1306 driver
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16

U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0, /* clock=*/ OLED_SCL, /* data=*/ OLED_SDA, /* reset=*/ OLED_RST); // Full framebuffer, SW I2C

char *str;
char *p ;
char sender[256];
char destination[256];
char random_numb[256];
char msg_ID[256];
char message[256];

/////////////////////Encryption////////////////////////
AESLib aesLib;
char cleartext[256];
char ciphertext[512];

// AES Encryption Key
byte aes_key[] = { 0xAC, 0xF2, 0xE1, 0xD7, 0xD7, 0xE6, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0xB8, 0x09, 0xF5, 0x4F, 0x7C };

// General initialization vector (you must use your own IV's in production for full security!!!)
byte aes_iv[N_BLOCK] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

// Generate IV (once)
void aes_init() {
  aesLib.gen_iv(aes_iv);
  // workaround for incorrect B64 functionality on first run...
  encrypt("inittttt", aes_iv);
}

String encrypt(char * msg, byte iv[]) {  
  int msgLen = strlen(msg);
  char encrypted[2 * msgLen];
  aesLib.encrypt64(msg, encrypted, aes_key, iv);  
  return String(encrypted);
}

String decrypt(char * msg, byte iv[]) {
  unsigned long ms = micros();
  int msgLen = strlen(msg);
  char decrypted[msgLen]; // half may be enough
  aesLib.decrypt64(msg, decrypted, aes_key, iv);  
  return String(decrypted);
}


/////////////////////////////////////////////////////

void setup() {
  Serial.begin(115200);
  while (!Serial); //if just the the basic function, must connect to a computer
  delay(1000);
  
  Serial.println("LoRa Receiver"); 
  
  SPI.begin(5,19,27,18);
  LoRa.setPins(SS,RST,DI0);

  Display.begin();
  Display.enableUTF8Print();    // enable UTF8 support for the Arduino print() function
  Display.setFont(u8g2_font_ncenB10_tr);
  
  if (!LoRa.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  /////CRC/////
  LoRa.crc();
  /////Encryption///////////////
  aes_init();
}

void loop() {

  bool read=false; // 0 si on n a pas recu de message
  // try to parse packet
  int packetSize = LoRa.parsePacket();
   // Serial.println("packetSize:" + String(packetSize));

  if (packetSize) {
    // received a packet
    Serial.println("-------------------------------------------------------------");
    Serial.println("Received packet ...");

    // read packet
    while (LoRa.available()) {
        String data = LoRa.readString();
    
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //-----------------Decrypt---------------------------------
        sprintf(ciphertext, "%s", data.c_str()); //c_str : recuperer le char* contenue dans un objet de type string.
        Serial.print("cipherrrrr: ");
        Serial.print(ciphertext);
        Serial.print("\n");
        
        byte dec_iv[N_BLOCK] = { 0xAA, 7, 0xFA, 0, 0xA, 0xAB, 0, 9, 0, 0, 0xBF, 0, 0xCA, 0, 0, 0xFF }; // iv_block gets written to, provide own fresh copy...
    
        String decrypted = decrypt(ciphertext, dec_iv);  
        Serial.print("Cleartext: ");
        Serial.println(decrypted);  
        
        //-----------------Convert string to char ------------------------
        // Length (with one extra character for the null terminator)
        int str_len = decrypted.length() + 1; 
        
        // Prepare the character array (the buffer) 
        char char_array[str_len];
        
        // Copy it over 
        decrypted.toCharArray(char_array, str_len);
    
        //--------------------Split message--------------------------
        int i=0,j=0; 
        p =char_array;
        while ((str = strtok_r(p, "_", &p)) != NULL)  // Don't use \n here it fails
        {
            //Serial.println("ici:");
            if (i==0)
                strcpy(sender, str); //WARNING!!
            if (i==1)
                strcpy(destination, str);
            if (i==2)
                strcpy(msg_ID, str);
            if (i==3)
               strcpy(random_numb, str);
            if (i==4)
               strcpy(message, str);
            //Serial.println(str);
            i++;
        }
        Serial.print("sender=");
        Serial.println(sender);
        Serial.print("dest=");
        Serial.println(destination);
        Serial.print("msg_ID=");
        Serial.println(msg_ID);
        Serial.print("random_numb=");
        Serial.println(random_numb);
        Serial.print("message=");
        Serial.println(message);
    
        //-----------------------Display-------------------------------------------
        Serial.print(data);
        
        Display.clearBuffer();  
        Display.setCursor(0,12); Display.print("LoRa Receiver");
        Display.setCursor(0,30); Display.print("Received Packet:");
        Display.setCursor(0,48); Display.print(" # " + decrypted);
        Display.sendBuffer();      

        read=true;
    }//while (LoRa.available())
    
    // print RSSI of packet
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());
  }//if(packetSize) 

    
    //-----------------------------ACK---------------------------------------------------
    if(read==true)
    {
      delay(2000);
      char text_ack[200];
      // send packet
      LoRa.beginPacket();
      //Serial.println("[ACK]Send from 0x" + String(sender) + " to 0x" + String(destination) + " pour le message:" +  String(msg_ID));
      sprintf(text_ack, "[ACK]Send from 0x%s to 0x%s #for the msg:%s",sender,destination,msg_ID); 
      Serial.println(text_ack);
      LoRa.print(text_ack);
      LoRa.endPacket();

      read=false;
   }

      
}
