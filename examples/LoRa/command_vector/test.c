#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 4

#define command_vector(name) \
		unsigned char name [SIZE/2]; \
		for (i = 0; i < SIZE/2; i++) \
			name[i] = 0xff;

#define command_vector_add(array, index, cmd) \
		if(index % 2){ \
			array[index/2] = array[index/2] & 0xf0 ; \
			array[index/2] |= cmd; \
		}else{ \
			array[index/2] = array[index/2] & 0x0f ; \
			array[index/2] |= cmd << 4; \
		}

#define command_vector_get(array, index, dest) \
		if(index % 2) \
			dest = array[index/2] & 0x0f; \
		else \
			dest = array[index/2] >> 4;


#define command_vector_print(array) \
		for (i = 0; i < SIZE/2; i++) \
			printf("0x%x \t", array[i]); \
		printf("\n");






int main(int argc, char const *argv[])
{
	int i;
	unsigned char dest;
	unsigned char *cmd_value = (unsigned char*) malloc (sizeof (unsigned char) * (SIZE/2));
	//unsigned char cmd_value [SIZE/2];
	command_vector(tableau)
	//unsigned char *tableau = (unsigned char*) malloc (sizeof (unsigned char) * (SIZE/2));

	command_vector_add(tableau, atoi(argv[1]), 0xa)

	command_vector_print(tableau)
	
	command_vector_get(tableau, atoi(argv[2]), dest);

	printf("Copying value\n");
	printf("sizeof (unsigned char) : %lu\n", sizeof (unsigned char));
	printf("sizeof (int) : %lu\n\n", sizeof (int));

	printf("%x\n", *tableau);
	printf("%x\n\n", *(tableau+1));


	printf("sizeof (tableau) : %lu\n", sizeof (tableau));
	printf("sizeof (cmd_value) : %lu\n", sizeof (cmd_value));

	//*cmd_value = tableau;
	//printf("first cmd_value : %x\n", *cmd_value);
	memcpy(cmd_value, tableau, 2);
	//printf("second cmd_value : %x\n\n", *cmd_value);

	printf("%x\n", *cmd_value);
	printf("%x\n\n", *(cmd_value+1));

	printf("%x\n", (short)*cmd_value);
	return 0;
}