#include <Arduino.h>

#include <SPI.h>
#include <LoRa.h>
#include <U8g2lib.h>

/* need button */
#define PIN_BUTTON  13
#define PIN_LED 2


#define SS      18
#define RST     14
#define DI0     26
#define BAND    433E6


// I2C OLED Display works with SSD1306 driver
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16


U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0, /* clock=*/ OLED_SCL, /* data=*/ OLED_SDA, /* reset=*/ OLED_RST); // Full framebuffer, SW I2C


volatile int button = 0;
volatile int led = 0;
TaskHandle_t led_task;
unsigned long old_time = 0;
char text[256];
unsigned char data_size = 0;


void IRAM_ATTR buttonPushed()
{
	Serial.println("buttonPushed");
	if (button)
		return;
	button = 1;
}

void LoRa_send_ack()
{
	/* return ack */
	LoRa.beginPacket();
	LoRa.write((uint8_t *)text, data_size-1);
	LoRa.endPacket();

	data_size ++;
}

void led_task_func(void *arg)
{
	if (led) {
		digitalWrite(PIN_LED, HIGH);
		delay(100);
		digitalWrite(PIN_LED, LOW);
		led = 0;
	} else {
		delay(100);
	}
}

void setup()
{
	Serial.begin(115200);
	while (!Serial); //if just the the basic function, must connect to a computer
  	
  	Serial.println("Serial ok");
	pinMode(PIN_LED, OUTPUT);
  	pinMode(PIN_BUTTON, INPUT);
  	attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), buttonPushed, RISING);

	/* https://github.com/sandeepmistry/arduino-LoRa/issues/83#issuecomment-362034767 */
	//pinMode(26, INPUT); 
	// not working : cause crash
	//attachInterrupt(digitalPinToInterrupt(26), LoRa_new_message, RISING);

	SPI.begin(5,19,27,18);
	LoRa.setPins(SS,RST,DI0);

	Display.begin();
	Display.enableUTF8Print();
	Display.setFont(u8g2_font_ncenB10_tr);

	LoRa.enableCrc();

	// cause crash
	//LoRa.onReceive(&LoRa_new_message);


	if (!LoRa.begin(BAND)) {
		Serial.println("Starting LoRa failed!");
		while (1);
	}
	Serial.println("LoRa Initial OK!");
	LoRa.crc();

#ifdef LED_TASK
	// multi core prog
  	xTaskCreatePinnedToCore(
                    led_task_func,   /* Task function. */
                    "led task",     /* name of task. */
                    1000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    0,           /* priority of the task */
                    &led_task,      /* Task handle to keep track of created task */
                    1);          /* pin task to core 0 */
	
	for (int i = 0; i < 255; ++i)
	{
		text[i] = '0';
	}
	text[255] = 0;
#endif

}



void loop()
{
	int new_time;
	if (button) {
		Serial.println("send packet");
		LoRa.beginPacket();
		LoRa.write((uint8_t *)text, 255);
		LoRa.endPacket();
		button = 0;
	}

	if (LoRa.parsePacket()) {
		new_time = millis();

		Serial.print(data_size);
		Serial.print(" ");
		Serial.print(new_time - old_time);
		Serial.print(" ");
		Serial.print((new_time - old_time+1)/(float)(data_size+1));
		Serial.print("\n\r");

		LoRa_send_ack();

		old_time = new_time;
	}
}
