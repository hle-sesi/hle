#include <AESLib.h>

#include <SPI.h>
#include <LoRa.h>
#include<Arduino.h>
#include <U8g2lib.h>   // https://github.com/olikraus/U8g2_Arduino

// GPIO5  -- SX1278's SCK
// GPIO19 -- SX1278's MISO
// GPIO27 -- SX1278's MOSI
// GPIO18 -- SX1278's CS
// GPIO14 -- SX1278's RESET
// GPIO26 -- SX1278's IRQ(Interrupt Request)

#define SS      18
#define RST     14
#define DI0     26
#define BAND    433E6  //915E6 -- 这里的模式选择中，检查一下是否可在中国实用915这个频段


// I2C OLED Display works with SSD1306 driver
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16

U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0, /* clock=*/ OLED_SCL, /* data=*/ OLED_SDA, /* reset=*/ OLED_RST); // Full framebuffer, SW I2C

#define NB_PLUGS 32
#define TYPE_PLUG_SB 0x2
byte type ;
byte sender;
byte msgCount;
byte buff[NB_PLUGS];

//uint4_t p;

///////Variables for ACK
byte type_0;//Plugs -> SB
byte sender_0;
byte msgCount_0;
byte buffer_0;
byte ack;




//Time interval
const unsigned long Interval = 1000;

/////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
      // initialize digital pin LED_BUILTIN as an output.
      pinMode(LED_BUILTIN, OUTPUT);
      //pinMode(25,OUTPUT); //Send success, LED will bright 1 second
  
      Serial.begin(115200);
      while (!Serial); //If just the the basic function, must connect to a computer
      
      SPI.begin(5,19,27,18);
      LoRa.setPins(SS,RST,DI0);
    //  Serial.println("LoRa Sender");
    
      Display.begin();
      Display.enableUTF8Print();    // enable UTF8 support for the Arduino print() function
      Display.setFont(u8g2_font_ncenB10_tr);
    
      if (!LoRa.begin(BAND)) {
        Serial.println("Starting LoRa failed!");
        while (1);
      }
      Serial.println("LoRa Initial OK!");
      /////CRC/////
      LoRa.crc();
      /////Encryption///////////////

      
  
}

void loop()
{
  task_listen(LoRa.parsePacket());
}

/////////////////
void task_cmd()
{
    switch (buff[0])
    {
      case 0x0 :
        
        Serial.println ("Plug working"); // Led switch ON
        //send acquittement
        send_ack();     
        break;
      
      case 0x1 :
        digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
        Serial.println ("ON"); // Led switch ON
        //send acquittement
        send_ack();     
        break;

       case 0x2 :
        digitalWrite(LED_BUILTIN, LOW);   // turn the LED on (HIGH is the voltage level)
        Serial.println ("OFF"); // Led switch OFF
        //send acquittement
        send_ack();
        break;

       default : 
        Serial.println("Other cases not define");
        break;

        
      }
    
  }
  
////////////////////////////////
void task_listen(int packetSize)
{
  if (packetSize == 0) return;
Serial.println("-------------------------------------------------------------------------------");

  //Read packet from SB
  type = LoRa.read();
  sender = LoRa.read();
  msgCount = LoRa.read();
  buff[NB_PLUGS];
  
/////////////Buffer Init///////////////
  for (int i=0; i<NB_PLUGS; i++){
    buff[i]=0;
}

/////////////Other Data Reception///////////////
  Serial.println("Received from: " + String(sender, HEX));
  Serial.println("Type: " + String(type));
  Serial.println("msgCount: " + String(msgCount));
  
/////////////Buffer Data Reception///////////////
  for (int i=0; i<NB_PLUGS; i++){
    //Serial.println(String(LoRa.read(), HEX));
    buff[i] = LoRa.read();
}
  
  task_cmd ();        
}

///////////////////
void send_ack()
{  
    Serial.println("---Sending ACK---"); 
    type_0= TYPE_PLUG_SB;//Plugs -> SB
    sender_0= 0;
    msgCount_0 = 0;
    buffer_0 = buff[0];
    ack = 0; // OK
  /////////////Send Packet///////////////
    LoRa.beginPacket ();
    LoRa.write (type_0);
    LoRa.write (sender_0);
    LoRa.write (msgCount_0);
    LoRa.write (buffer_0);
    LoRa.write (ack);
    LoRa.endPacket ();
    msgCount ++;      
  
  }
