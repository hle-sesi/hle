#include <AESLib.h>

#include <SPI.h>
#include <LoRa.h>
#include<Arduino.h>
#include <U8g2lib.h>   // https://github.com/olikraus/U8g2_Arduino




// GPIO5  -- SX1278's SCK
// GPIO19 -- SX1278's MISO
// GPIO27 -- SX1278's MOSI
// GPIO18 -- SX1278's CS
// GPIO14 -- SX1278's RESET
// GPIO26 -- SX1278's IRQ(Interrupt Request)

#define SS      18
#define RST     14
#define DI0     26
#define BAND    433E6  //915E6 -- 这里的模式选择中，检查一下是否可在中国实用915这个频段


// I2C OLED Display works with SSD1306 driver
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16

U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0, /* clock=*/ OLED_SCL, /* data=*/ OLED_SDA, /* reset=*/ OLED_RST); // Full framebuffer, SW I2C


int counter = 0;

/////////////////////Encryption////////////////////////
AESLib aesLib;
char cleartext[256];
char ciphertext[512];

// AES Encryption Key
byte aes_key[] = { 0xAC, 0xF2, 0xE1, 0xD7, 0xD7, 0xE6, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0xB8, 0x09, 0xF5, 0x4F, 0x7C };

// General initialization vector (you must use your own IV's in production for full security!!!)
byte aes_iv[N_BLOCK] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

// Generate IV (once)
void aes_init() {
  aesLib.gen_iv(aes_iv);
  // workaround for incorrect B64 functionality on first run...
  encrypt("HELLO WORLD!", aes_iv);
}

String encrypt(char * msg, byte iv[]) {  
  int msgLen = strlen(msg);
  char encrypted[2 * msgLen];
  aesLib.encrypt64(msg, encrypted, aes_key, iv);  
  return String(encrypted);
}

String decrypt(char * msg, byte iv[]) {
  unsigned long ms = micros();
  int msgLen = strlen(msg);
  char decrypted[msgLen]; // half may be enough
  aesLib.decrypt64(msg, decrypted, aes_key, iv);  
  return String(decrypted);
}


/////////////////////////////////////////////////////

void setup() {
  pinMode(25,OUTPUT); //Send success, LED will bright 1 second
  
  Serial.begin(115200);
  while (!Serial); //If just the the basic function, must connect to a computer
  
  SPI.begin(5,19,27,18);
  LoRa.setPins(SS,RST,DI0);
//  Serial.println("LoRa Sender");


  Display.begin();
  Display.enableUTF8Print();    // enable UTF8 support for the Arduino print() function
  Display.setFont(u8g2_font_ncenB10_tr);

  if (!LoRa.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  Serial.println("LoRa Initial OK!");
  /////CRC/////
  LoRa.crc();
  /////Encryption///////////////
    aes_init();

}

void loop() {
  Serial.print("Sending packet: ");
  Serial.println(counter);

  // send packet
  LoRa.beginPacket();
  //LoRa.print("hello ");
  ////////////////////////////////////
  sprintf(cleartext, "hilal \n");  

  // Encrypt
  byte enc_iv[N_BLOCK] = { 0xAA, 7, 0xFA, 0, 0xA, 0xAB, 0, 9, 0, 0, 0xBF, 0, 0xCA, 0, 0, 0xFF }; // iv_block gets written to, provide own fresh copy...
  String encrypted = encrypt(cleartext, enc_iv);
  //sprintf(ciphertext, "%s", encrypted.c_str());
  Serial.print("Ciphertext: ");
  Serial.println(encrypted);

  LoRa.print(encrypted);

  /*// Decrypt
  byte dec_iv[N_BLOCK] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // iv_block gets written to, provide own fresh copy...
  String decrypted = decrypt(ciphertext, dec_iv);  
  Serial.print("Cleartext: ");
  Serial.println(decrypted);  */
  
  ////////////////////////////////
  //LoRa.print(counter);
  
  LoRa.endPacket();

  Display.clearBuffer();  
  Display.setCursor(0,12); Display.print("LoRa Sender");
  Display.setCursor(0,30); Display.print("Sent Packet:");
  Display.setCursor(0,48); Display.print(" # " + (String)counter);
  Display.sendBuffer();

  counter++;
  digitalWrite(25, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(25, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
  
  delay(3000);
}
