#include <AESLib.h>

#include <SPI.h>
#include <LoRa.h>
#include<Arduino.h>
#include <U8g2lib.h>   // https://github.com/olikraus/U8g2_Arduino

// GPIO5  -- SX1278's SCK
// GPIO19 -- SX1278's MISO
// GPIO27 -- SX1278's MOSI
// GPIO18 -- SX1278's CS
// GPIO14 -- SX1278's RESET
// GPIO26 -- SX1278's IRQ(Interrupt Request)

#define SS      18
#define RST     14
#define DI0     26
#define BAND    433E6  //915E6 -- 这里的模式选择中，检查一下是否可在中国实用915这个频段


// I2C OLED Display works with SSD1306 driver
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16

U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0, /* clock=*/ OLED_SCL, /* data=*/ OLED_SDA, /* reset=*/ OLED_RST); // Full framebuffer, SW I2C


int counter = 0;
long random_numb = 0;
long tab[4];

int sender;
int destination;
int msg_ID1=0;
int msg_ID2=0;

bool wait_ack1=false;
bool wait_ack2=false;

int k1=0,k2=0;

///////////////////////////Encryption////////////////////////
AESLib aesLib;
char cleartext[256];
char ciphertext[512];

// AES Encryption Key
byte aes_key[] = { 0xAC, 0xF2, 0xE1, 0xD7, 0xD7, 0xE6, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0xB8, 0x09, 0xF5, 0x4F, 0x7C };

// General initialization vector (you must use your own IV's in production for full security!!!)
byte aes_iv[N_BLOCK] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

// Generate IV (once)
void aes_init() {
  aesLib.gen_iv(aes_iv);
  // workaround for incorrect B64 functionality on first run...
  encrypt("HELLO WORLD!", aes_iv);
}

String encrypt(char * msg, byte iv[]) {  
  int msgLen = strlen(msg);
  char encrypted[2 * msgLen];
  aesLib.encrypt64(msg, encrypted, aes_key, iv);  
  return String(encrypted);
}

String decrypt(char * msg, byte iv[]) {
  unsigned long ms = micros();
  int msgLen = strlen(msg);
  char decrypted[msgLen]; // half may be enough
  aesLib.decrypt64(msg, decrypted, aes_key, iv);  
  return String(decrypted);
}
/////////////////////////////////////////////////////
//Time interval
const unsigned long Interval = 1000;

/////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
    if( (wait_ack1==false) )
    {
      pinMode(25,OUTPUT); //Send success, LED will bright 1 second
  
      Serial.begin(115200);
      while (!Serial); //If just the the basic function, must connect to a computer
      
      SPI.begin(5,19,27,18);
      LoRa.setPins(SS,RST,DI0);
    //  Serial.println("LoRa Sender");
    
      Display.begin();
      Display.enableUTF8Print();    // enable UTF8 support for the Arduino print() function
      Display.setFont(u8g2_font_ncenB10_tr);
    
      if (!LoRa.begin(BAND)) {
        Serial.println("Starting LoRa failed!");
        while (1);
      }
      Serial.println("LoRa Initial OK!");
      /////CRC/////
      LoRa.crc();
      /////Encryption///////////////
        aes_init();
       k1=0;
  }
}

void loop()
{
  task_cmd();
  task_listen();
}

void task_cmd()
{
  static unsigned long previousMillis = 0;
  unsigned long currentMillis = millis();
  
  if(currentMillis - previousMillis >= Interval)   
    {
        previousMillis = currentMillis;
        
                Serial.println("--------------------------------------------------------------------------------");
        Serial.print("Sending packet: ");
        Serial.println(msg_ID1);
        ////////////////////////////////////
        //-------------------Send packet--------------------------------
        LoRa.beginPacket();
        
        sender = 1;
        destination = 1;
        random_numb = random(0, 50);
        sprintf(cleartext, "Packet_%d_%d_%d_cmd1 \n",sender,destination,msg_ID1);  
        //sprintf(cleartext, "Packet_%d_%d_%d_%d_cmd1 \n",sender,destination,msg_ID1,random_numb);  
        Serial.println("Msg sent : " + String( cleartext));
        //----------------- Encrypt---------------------------
        byte enc_iv[N_BLOCK]= { 0xAA, 7, 0xFA, 0, 0xA, 0xAB, 0, 9, 0, 0, 0xBF, 0, 0xCA, 0, 0, 0xFF }; // iv_block gets written to, provide own fresh copy...;
      
        String encrypted = encrypt(cleartext, enc_iv);
        Serial.print("Ciphertext: ");
        Serial.println(encrypted);
      
        LoRa.print(encrypted);
        LoRa.endPacket();
        //---------------------Display-------------------------------------
        Display.clearBuffer();  
        Display.setCursor(0,12); Display.print("LoRa Sender");
        Display.setCursor(0,30); Display.print("Sent Packet:");
        Display.setCursor(0,48); Display.print(" # " + (String)msg_ID1);
        Display.sendBuffer();
        
    }
}

void task_listen()
{
    //Serial.println("**ecoute**");
    //------------------If wait the ack------------------------------
    //if( (wait_ack1==true) )
    //{
      //Serial.println("Ecoute...");
      String data;
      char compare1[200]="[ACK]Send from 0x1 to 0x1 #for the msg:";
      char buff_msg_ID[200];
      
      sprintf(buff_msg_ID,"%d",(msg_ID1-1)); //int -> char*
      //Serial.println("buff: " + String(buff_msg_ID));
      strcat(compare1,buff_msg_ID);
      
      //Serial.println("**ecoute**");
      //-------------------ACK----------------------------------------
      // try to parse packet
      int packetSize = LoRa.parsePacket();
      if (packetSize) {
        // received a packet
        Serial.print("[ACK] Received packet : ");
    
        // read packet
        while (LoRa.available()) {
          //Serial.print((char)LoRa.read());
          data = LoRa.readString();
          Serial.println(data);
        }
       //-----------------Convert string to char ------------------------
          // Length (with one extra character for the null terminator)
          int str_len = data.length() + 1; 
          
          // Prepare the character array (the buffer) 
          char char_array[str_len];
          
          // Copy it over 
          data.toCharArray(char_array, str_len);
      /*
       //--------------------Split message--------------------------
          int i=0,j=0;
          char* str;
          char* p =char_array;
          char ack_msg[200];
          while ((str = strtok_r(p, "#", &p)) != NULL)  // Don't use \n here it fails
          {
              if (i==0)
              {
                strcpy(ack_msg,str); //for the first part of the message 
              }
              i++;
          }
          Serial.println("[ACK] Split : " + String(ack_msg));
        */  
        //-----------------------Comparison---------------------
          if ( (strcmp(char_array,compare1)==0) && (wait_ack1==true) ) //if  identical
          {
            wait_ack1=false;//we don t wait anymore ACK
            Serial.println("[ACK] Identical for packet 1!!");
            //Serial.println(compare1);
          }
          else
          {
            Serial.println("[ACK] Not identical for packet 1...");
            //Serial.println(compare1);
            wait_ack1=true;
          }
  
        }//if (packetSize)  
      
    //} 
}
