
#define RX_PIN 16
#define TX_PIN 17


// this is a large buffer for replies
char replybuffer[255];

void setup() {
	Serial2.begin(9600, SERIAL_8N1, RX_PIN, TX_PIN);
	Serial.begin(115200);
	Serial.println("Serial reading...");
}


void loop() {
	int avail = 0;
	while (Serial2.available()) {
    	avail = 1;
    	char c = Serial2.read();
    	Serial.print(c);
  	}
  	if (avail) {
  		Serial.println("");
  		Serial2.println("OK");
  	}
  	delay(100);
}