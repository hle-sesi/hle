//SIM800_Test_OK_pour_Ctrl-z
//#include <SoftwareSerial.h>
//Envoie une ligne tapée au SIM800
//En fin de SMS, le Ctrl-z indispensable est censuré par l'IDE Arduino
//on le remplace donc par le caratère "&".

//SIM800 TX connecté à  Arduino D10
#define SIM800_TX_PIN 17

//SIM800 RX  connecté à  Arduino D11
#define SIM800_RX_PIN 16
int a = 0;
char c;//Reçoit le char tapé 
//Create software serial object to communicate with SIM800
HardwareSerial *serialSIM800 = &Serial2;

void setup() {
  //Begin serial comunication with Arduino and Arduino IDE (Serial Monitor)
  Serial.begin(115200);
  while (!Serial);

  //Being serial communication witj Arduino and SIM800
  serialSIM800->begin(115200);
  if (!*serialSIM800) {
    Serial.println("Seiral2 fail");
    while(1);
  }
  delay(1000);
  Serial.println("Setup Complete!");
}

void loop()
{
  //Read SIM800 output (if available) and print it in Arduino IDE Serial Monitor
  if (serialSIM800->available()) {
    Serial.write(serialSIM800->read());
  }
  //Read Arduino IDE Serial Monitor inputs (if available) and send them to SIM800
  if (Serial.available()) {
    c = Serial.read(); //& remplace Ctrl-z, fin de sms
    if (c == '&') c=(char)26;//Envoyer Ctrl-z
    serialSIM800->write(c);
    //Serial.print(c);
  }
}
