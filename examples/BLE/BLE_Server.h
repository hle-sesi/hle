#ifndef __BLE_SODI_SERVER_H__
#define __BLE_SODI_SERVER_H__
#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

#include "global_var.h"
#include "Queue.h"
#include "base_station.h"

//CONFIG ble
#define SERV_NAME "HLE SERVER"
#define ESP_VERSION_1_00

//Service define
#define SERV_GENERIC_ACCESS             ((uint16_t)0x1800)
#define SERV_HUMAIN_INTERFACE_DEVICE    ((uint16_t)0x1812)


//Characteristic define
#define CHAR_DEVICE_NAME          ((uint16_t)0x2A00)
#define CHAR_USER_CONTROL_POINT   ((uint16_t)0x2A9F)
#define CHAR_REPORT               ((uint16_t)0x2A4D)
#define CHAR_HID_CONTROL_POINT    ((uint16_t)0x2A4C)
#define CHAR_STRING               ((uint16_t)0x2A3D)

extern boolean BLE_connected;
extern BLECharacteristic *BLE_Characteristic[];
extern BLEService *BLE_Service[];
extern BLEServer *BLE_Server;
extern BLEUUID servUUID[];
extern BLEUUID charUUID[];

extern hle::Queue<command*> normal_queue;

class SERV_CONNECTION_CALLBACK;
class CMD_CALLBACK;

void BLE_init();
void BLE_advertise();
void BLE_update(unsigned char id_src, 
    unsigned char cmd, unsigned char status);


#endif