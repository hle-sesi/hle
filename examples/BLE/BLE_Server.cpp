#include <Arduino.h>
#include <string.h>
#include "BLE_Server.h"
#include "init_var.h"

BLECharacteristic *BLE_Characteristic[3];
BLEService *BLE_Service[3];
BLEServer *BLE_Server;
boolean BLE_connected = false;

BLEUUID servUUID[] = {
	BLEUUID(SERV_GENERIC_ACCESS),
	BLEUUID("beb5481e-36e1-4688-b7f5-ea07361b26a8")
};

BLEUUID charUUID[] = {
	BLEUUID("beb5483e-36e1-4688-b7f5-ea07361b26a8"),
	BLEUUID("beb5484e-36e1-4688-b7f5-ea07361b26a8"),
	BLEUUID("beb5485e-36e1-4688-b7f5-ea07361b26a8"),
	BLEUUID("beb5486e-36e1-4688-b7f5-ea07361b26a8")
};

class SERV_CONNECTION_CALLBACK : public BLEServerCallbacks {
    void onConnect(BLEServer* pserver) {
        BLE_connected = true;
        Serial.println("onConnect");
    }

    void onDisconnect(BLEServer* pserver) {
        BLE_connected = false;
        Serial.println("onDisconnect");
    }
};

class CMD_CALLBACK : public BLECharacteristicCallbacks 
{
    virtual void onRead(BLECharacteristic* pCharacteristic)
    {}

    virtual void onWrite(BLECharacteristic* pCharacteristic)
    {
      //byte current_command[COMMAND_VECTOR_SIZE];

        Serial.print("data of: ");
        Serial.print(pCharacteristic->getUUID().toString().c_str());
        Serial.print(", value : ");
        Serial.println(pCharacteristic->getValue().c_str());
        for (int i = 0; i < pCharacteristic->getValue().length(); ++i)
        {
            Serial.println((unsigned char)pCharacteristic->getValue().c_str()[i]);
        }

        /* TODO need to create a full command and push it into normal_queue */

        command *cmd = new command;
        cmd->prog_id = 0; // 0 for BLE
        cmd->type = CMD;
        cmd->sender = BASE_STATION_ID;
        cmd->msg_id = -1;
        cmd->cmd_vector = {0x2, 0x2, 0x2, 0x2};

        normal_queue.push(cmd);
        delete [] cmd;
        /*
        char *record = new char[4];
        record[0] = ID_BLE;
        record[1] = (char)pCharacteristic->getValue().c_str()[1];
        record[2] = (char)pCharacteristic->getValue().c_str()[2];
        LoRa_toSend.push(record);

        BLE_update(record[1], record[2], 1);
        Serial.println("report is set");
        delete [] record;
        */
    }
};

class REPORT_CALLBACK : public BLECharacteristicCallbacks 
{
    virtual void onRead(BLECharacteristic* pCharacteristic)
    {
    	Serial.println("report read");
    	BLE_Characteristic[1]->setValue("Test");
    }

    virtual void onWrite(BLECharacteristic* pCharacteristic)
    {
        Serial.println("report wrote");
    }
};

void BLE_init()
{
  /*Serial.println("MTU lenght :");
  mtu = BLEDevice::getMTU ();
  Serial.println(mtu);
  set=BLEDevice::setMTU (500);
  mtu1 = BLEDevice::getMTU ();
  Serial.println(mtu1);*/

#ifdef SERV_NAME
	BLEDevice::init(SERV_NAME);
#else
	BLEDevice::init("ESP32");
#endif

	BLE_Server = BLEDevice::createServer();
  	BLE_Server->setCallbacks(new SERV_CONNECTION_CALLBACK());
	BLE_Service[0] = BLE_Server->createService(SERV_HUMAIN_INTERFACE_DEVICE);

	BLE_Characteristic[0] = BLE_Service[0]->createCharacteristic(
		CHAR_USER_CONTROL_POINT,
		BLECharacteristic::PROPERTY_WRITE);
	
	BLE_Characteristic[0]->setCallbacks(new CMD_CALLBACK());

	BLE_Characteristic[1] = BLE_Service[0]->createCharacteristic(
		CHAR_STRING,
		BLECharacteristic::PROPERTY_READ);

	BLE_Characteristic[1]->setValue("init");

	BLE_Service[0]->start();
}


void BLE_advertise()
{

#ifdef ESP_VERSION_1_02
  	BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  	pAdvertising->setScanResponse(true);
  	pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
#endif

#ifdef ESP_VERSION_1_00
  	BLEAdvertising *pAdvertising = BLE_Server->getAdvertising();
  	//Don't set minInterval to 0x06, it will cause advertise issues.
#endif

#ifdef ESP_VERSION_1_02
    BLEDevice::startAdvertising();
#endif
    pAdvertising->addServiceUUID(SERV_HUMAIN_INTERFACE_DEVICE);

#ifdef ESP_VERSION_1_00
	pAdvertising->start();
#endif

	Serial.println("Characteristic defined! Now you can read data in ESP client!");
}

void BLE_update(unsigned char id_src, 
	unsigned char cmd, unsigned char status)
{
	char buff[255];
	snprintf(buff, 255, "Status de la commande %d du Satellite %d : %x", 
		cmd, id_src, status);
	BLE_Characteristic[1]->setValue(buff);
}