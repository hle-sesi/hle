#include "RTClib.h"       //to show time -- https://github.com/adafruit/RTClib
#include <Wire.h>  // for I2C with RTC module

#define WEEKEND_OPEN_HOUR   9
#define WEEKEND_OPEN_MIN    0
#define WEEKEND_CLOSE_HOUR  1
#define WEEKEND_CLOSE_MIN   0

#define WEEKDAYS_OPEN_HOUR  6
#define WEEKDAYS_OPEN_MIN   30
#define WEEKDAYS_CLOSE_HOUR 0
#define WEEKDAYS_CLOSE_MIN  0

RTC_DS1307 rtc;

char days_of_Week[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};










void setup() {
	Serial.begin(9600);
	
	if (! rtc.begin()) {
		Serial.println("Couldn't find RTC");
		while (1);
	}
	if (! rtc.isrunning()) {
		Serial.println("RTC is NOT running!");
		rtc.adjust(DateTime(__DATE__, __TIME__)); //set the time according to our PC time
 	}
}

void print_time(DateTime now)
{
	Serial.print(now.year(), DEC);
	Serial.print('/');
	Serial.print(now.month(), DEC);
	Serial.print('/');
	Serial.print(now.day(), DEC);
	Serial.print(" (");
	Serial.print(days_of_Week[now.dayOfTheWeek()]);
	Serial.print(") ");
	Serial.print(now.hour(), DEC);
	Serial.print(':');
	Serial.print(now.minute(), DEC);
	Serial.print(':');
	Serial.print(now.second(), DEC);
	Serial.println();
	delay(3000);
}

void planning (DateTime now, int mode)
{
	if (mode == 1) //planning1: Sunday-Saturday
	{
		if ( (now.hour()==WEEKEND_OPEN_HOUR) && (now.minute()==WEEKEND_OPEN_MIN) ) //if 9h00 -> open
		{
			Serial.println("it's morning!");
		}
		if ( (now.hour()==WEEKEND_CLOSE_HOUR) && (now.minute()==WEEKEND_CLOSE_MIN) ) //if 01h00 -> close
		{
			Serial.println("it's night!");
		}
	}

	if (mode == 2) //planning2: Monday-to-Friday
	{
		if ( (now.hour()==WEEKDAYS_OPEN_HOUR) && (now.minute()==WEEKDAYS_OPEN_MIN) ) //if 6h30 -> open
		{
			Serial.println("it's morning!");
		}
		if ( (now.hour()==WEEKDAYS_CLOSE_HOUR) && (now.minute()==WEEKDAYS_CLOSE_MIN) ) //if 00h00 -> close
		{
			Serial.println("it's night!");
		}
	}
}

void loop() {
	DateTime now = rtc.now();

	//Serial.println(days_of_Week[now.dayOfTheWeek()]);
	if( (strcmp(days_of_Week[now.dayOfTheWeek()],"Sunday") == 0) || 
		(strcmp(days_of_Week[now.dayOfTheWeek()],"Saturday") == 0)) //if identical
	{
		Serial.println("---------------------------------------");
		Serial.println("--planning_1--");
		planning(now, 1);
	}else{   
		Serial.println("---------------------------------------");
		Serial.println("--planning_2--");
		planning(now, 2);
	}
	
	print_time(now);

}