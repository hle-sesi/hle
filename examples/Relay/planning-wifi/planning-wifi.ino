/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com
  Based on the NTP Client library example
*********/

#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <string.h>

/* Pins associated with relays */
const int relay1 = 0;
const int relay2 = 17;
const int relay3 = 0;
const int relay4 = 0;


/* network info for connection */
const char* ssid     = "dd-wrt";
const char* password = "peri2019";

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

/* Variables to save date and time */
String formattedDate;
String dayStamp;
String timeStamp;
char hour[8];
/* ------------------------------------------------------ SETUP ------------------------------------------------------ */
void setup() {
  /* Initialize Serial Monitor */
  Serial.begin(115200);
  
  /* Initialazing pin for relays */
  pinMode(relay2, OUTPUT);
  //digitalWrite(relay2, HIGH);

  
  /* Connection to network */
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  /*Initialize a NTPClient and timezone */
  timeClient.begin();
  timeClient.setTimeOffset(3600);
}


/* ------------------------------------------------------ TRY_HOUR ------------------------------------------------------ */
int try_hour(char * hour){
  if(!strcmp(hour, "01:00:05")){
    digitalWrite(relay2, LOW);    // NC
    Serial.println("first low");
  }
  
  if(!strcmp(hour, "01:00:10")){
    digitalWrite(relay2, HIGH);   // NO
    Serial.println("first high");
  }
  
  if(!strcmp(hour, "01:00:15")){
    digitalWrite(relay2, LOW);    // NC
    Serial.println("second low");
  }  

  if(!strcmp(hour, "01:00:17")){
    digitalWrite(relay2, HIGH);    // NC
    Serial.println("second high");
  }  
  
  //digitalWrite(relay2, HIGH);   // NO
  Serial.print("Pin value : ");
  Serial.println(digitalRead(relay2), DEC);
  Serial.println();
  
  return 0;
}


/* ------------------------------------------------------ LOOP ------------------------------------------------------ */
void loop() {
    Serial.println("loop start");
  /* 
  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }
  */
  // The formattedDate comes with the following format:
  // 2018-05-28T16:00:13Z
  // We need to extract date and time
  formattedDate = timeClient.getFormattedDate();
  Serial.println(formattedDate);

  // Extract date
  int splitT = formattedDate.indexOf("T");
  dayStamp = formattedDate.substring(0, splitT);
  Serial.print("DATE: ");
  Serial.println(dayStamp);
  // Extract time
  timeStamp = formattedDate.substring(splitT+1, formattedDate.length()-1);
  Serial.print("HOUR: ");
  Serial.println(timeStamp);
  strcpy(hour, timeStamp.c_str());
  try_hour(hour);
  delay(1000);
}
