#include <Wire.h>
#include <Adafruit_INA219.h>
#include <U8g2lib.h>   // https://github.com/olikraus/U8g2_Arduino
#include <SPI.h>

#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16

#define TAB_SZ 1024

Adafruit_INA219 ina219;
U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0, /* clock=*/ OLED_SCL, /* data=*/ OLED_SDA, /* reset=*/ OLED_RST); // Full framebuffer, SW I2C


float value_tab[TAB_SZ];
int ptr_w = 0;

void setup(void) 
{
  Serial.begin(115200);
  while (!Serial) {
      // will pause Zero, Leonardo, etc until serial console opens
      delay(1);
  }

  uint32_t currentFrequency;
    
  Serial.println("Hello!");
  
  // Initialize the INA219.
  // By default the initialization will use the largest range (32V, 2A).  However
  // you can call a setCalibration function to change this range (see comments).
  ina219.begin();
  // To use a slightly lower 32V, 1A range (higher precision on amps):
  //ina219.setCalibration_32V_1A();
  // Or to use a lower 16V, 400mA range (higher precision on volts and amps):
  //ina219.setCalibration_16V_400mA();

  Serial.println("Measuring voltage and current with INA219 ...");
  Display.begin();
  Display.enableUTF8Print();    // enable UTF8 support for the Arduino print() function
  Display.setFont(u8g2_font_5x8_tf);
  

}

/*
 * compute n value
 */
float average(int nb_value, float last_value=0)
{
  float ret = last_value;
  int ptr_r = ptr_w - nb_value;

  if (ptr_r < 0) {
    ptr_r = TAB_SZ - 1 - ptr_r;
  }
    
  if (last_value >= 0.01) {
    ret = ret - value_tab[ptr_r] + value_tab[ptr_w];
  } else {
    for (int i = 0; i < nb_value ; i++) {
      ret += value_tab[(ptr_r++)%TAB_SZ];
    }
  }


  return ret/nb_value;
}

float last_100_value = 0;
float last_10_value = 0;
void loop(void) 
{
  float shuntvoltage = 0;
  float busvoltage = 0;
  float current_mA = 0;
  float loadvoltage = 0;
  float power_mW = 0;

  shuntvoltage = ina219.getShuntVoltage_mV();
  busvoltage = ina219.getBusVoltage_V();
  current_mA = ina219.getCurrent_mA();
  power_mW = ina219.getPower_mW();
  loadvoltage = busvoltage + (shuntvoltage / 1000);
  /*
  Serial.print("Bus Voltage:   "); Serial.print(busvoltage); Serial.println(" V");
  Serial.print("Shunt Voltage: "); Serial.print(shuntvoltage); Serial.println(" mV");
  Serial.print("Load Voltage:  "); Serial.print(loadvoltage); Serial.println(" V");
  Serial.print("Current:       "); 
  */
  Serial.print(current_mA); //Serial.println(" mA");
  if (++ptr_w >= TAB_SZ) {
    ptr_w = 0;
  }
  value_tab[ptr_w] = current_mA;
  //Serial.print("Power:         "); Serial.print(power_mW); Serial.println(" mW");
  
  Serial.print(" ");

  last_10_value = average(10);
  Serial.print(last_10_value);
  Serial.print(" ");

  last_100_value = average(100);
  Serial.print(last_100_value);
  
  
  Serial.println("\t");
  Display.clearBuffer();
  Display.setCursor(0,6);  Display.print("current : " + String(current_mA) + " mA");
  Display.setCursor(0,13); Display.print("  shunt volt: " + String(shuntvoltage) + " V");
  Display.setCursor(0,21); Display.print("    bus volt: " + String(busvoltage) + " V");
  Display.setCursor(0,30); Display.print("   load volt: " + String(loadvoltage) + " V");
  Display.setCursor(0,39); Display.print("avg 10 val : " + String(last_10_value) + " mA");  
  Display.setCursor(0,48); Display.print("avg 100 val : " + String(last_100_value) + " mA");
  Display.sendBuffer();

  delay(50);
}
