#ifndef __BUFFER_LORA_H__
#define __BUFFER_LORA_H__

#include <cstdlib>
#include <iostream>

#define LORA_MAX_BUFFER_SIZE 10
#define inc(var) (var+1)%max_nb_elements ? var+1 : 0;

namespace hle
{

template <typename T>
class Queue
{
public:
	Queue(std::size_t max_nb_elements=LORA_MAX_BUFFER_SIZE);
	~Queue();
	inline int push(T &element);
	inline int push(const T &element);
	inline T peek() const;
	inline T pop();

	/* getter and setter */

	inline T* getData(int &nb_elements) const;
	inline T* getData() const;
	inline int empty() const;
	inline int full() const;
	inline size_t count() const;

private:
	T* data;
	size_t max_nb_elements;
	size_t ptr_r, ptr_w;
};

	template<typename T>
	Queue<T>::Queue(std::size_t max_nb_elements)
		: max_nb_elements(max_nb_elements)
		, ptr_r(0), ptr_w(0)
	{
		data = new T[max_nb_elements];
	}

	template<typename T>
	Queue<T>::~Queue()
	{
		delete [] data;
	}

	template<typename T>
	int Queue<T>::push(T &element)
	{
		if (full())
			return -1;
		data[ptr_w] = element;
		ptr_w = inc(ptr_w);
		return 0;
	}

	template<typename T>
	int Queue<T>::push(const T &element)
	{
		if (full())
			return -1;
		data[ptr_w] = element;
		ptr_w = inc(ptr_w);
		return 0;
	}

	template<typename T>
	T Queue<T>::peek() const
	{
		if (empty())
			return NULL;
		else
			return data[ptr_r];
	}

	template<typename T>
	T Queue<T>::pop()
	{
		T ret = NULL;
		if (empty())
			return NULL;

		ret = data[ptr_r];
		ptr_r = inc(ptr_r);
		return ret;
	}

	template<typename T>
	T* Queue<T>::getData() const
	{
		return data;
	}

	template<typename T>
	T* Queue<T>::getData(int &nb_elements) const
	{
		nb_elements = count();
		return data;
	}

	/*
	 *  ptr_r
	 *  v
	 * +-------+
	 * | | | | |
	 * +-------+
	 *  ^
	 *  ptr_w
	 */
	template<typename T>
	int Queue<T>::empty() const
	{
		if (ptr_w == ptr_r)
			return 1;
		else
			return 0;
	}

	/*
	 *  ptr_r
	 *    v
	 * +-------+
	 * | | | | |
	 * +-------+
	 *  ^  
	 *  ptr_w
	 */
	template<typename T>
	int Queue<T>::full() const
	{
		if ((ptr_w+1)%max_nb_elements == ptr_r)
			return 1;
		else
			return 0;
	}

	template<typename T>
	size_t Queue<T>::count() const
	{
		if (ptr_r > ptr_w)
			return max_nb_elements - ptr_r + ptr_w;
		else
			return ptr_w - ptr_r;
	}

} //namespace hle

#undef inc

#endif