#include "base_station.h"
#include "BLE_Server.h"
using namespace hle;

/* command to send variables */
byte current_msg_id = 0;
byte last_sent_msg_id = -1;
command *current_command;

/* timer for timeout variables */
volatile int timeout_interrupt_counter = 0;
//int total_timeout_interrupt_counter = 0; /* TODO usefull? */
hw_timer_t * timeout_timer = NULL;
portMUX_TYPE timeout_timer_mux = portMUX_INITIALIZER_UNLOCKED;


/* timer for planning variables */
volatile int planning_interrupt_counter = 0;
//int total_planning_interrupt_counter = 0; /* TODO usefull? */
hw_timer_t * planning_timer = NULL;
portMUX_TYPE planning_timer_mux = portMUX_INITIALIZER_UNLOCKED;


/* planning variables */
RTC_DS1307 rtc;
char days_of_Week[7][12] = {"Sunday", 
														"Monday", 
														"Tuesday", 
														"Wednesday", 
														"Thursday", 
														"Friday", 
														"Saturday"};


/* buffers declaration */
Queue<command*> urgent_queue(10);
Queue<command*> normal_queue(10);
Queue<command*> current_command_queue(1);



/*==================================================================*/
void push_cmd_to_normal(unsigned char cmd_value_for_all)
{
	command *new_cmd = new command;
	new_cmd->prog_id = PLANNING; /* TODO only used for planning? */
	new_cmd->type = CMD;
	new_cmd->sender = BASE_STATION_ID;
	new_cmd->msg_id = current_msg_id;
	current_msg_id ++;

	for (int i = 0; i < TOTAL_NB_PLUGS; i++)
	{
		command_vector_add(new_cmd->cmd_vector, i, cmd_value_for_all)
	}

	normal_queue.push(new_cmd);
	/* TODO free new_cmd */
}
/*==================================================================*/



/*********************************************************************/
void check_planning()
{
	Serial.println("Trying planning");
	DateTime now = rtc.now();

	if(planning_interrupt_counter > 0){
		portENTER_CRITICAL(&planning_timer_mux);
		planning_interrupt_counter--;
		portEXIT_CRITICAL(&planning_timer_mux);

		/*
		total_planning_interrupt_counter++;
		Serial.print("Interrupt on planning. Total number: ");
		Serial.println(total_planning_interrupt_counter);
		*/

		if((strcmp(days_of_Week[now.dayOfTheWeek()],"Sunday") == 0) || 
			(strcmp(days_of_Week[now.dayOfTheWeek()],"Saturday") == 0))
		{
			if ((now.hour()==WEEKEND_OPEN_HOUR) &&
				(now.minute()==WEEKEND_OPEN_MIN))
			{
				Serial.println("it's morning!");
				/* send CMD_ON to all plugs */
				push_cmd_to_normal(CMD_ON);
			}

			if ((now.hour()==WEEKEND_CLOSE_HOUR) &&
				(now.minute()==WEEKEND_CLOSE_MIN))
			{
				Serial.println("it's night!");
				/* send CMD_OFF to all plugs */
				push_cmd_to_normal(CMD_OFF);
			}
		}else{
			if ((now.hour()==WEEKDAYS_OPEN_HOUR) &&
				(now.minute()==WEEKDAYS_OPEN_MIN))
			{
				Serial.println("it's morning!");
				/* send CMD_ON to all plugs */
				push_cmd_to_normal(CMD_ON);
			}
			
			if ((now.hour()==WEEKDAYS_CLOSE_HOUR) &&
				(now.minute()==WEEKDAYS_CLOSE_MIN))
			{
				Serial.println("it's night!");
				/* send CMD_OFF to all plugs */
				push_cmd_to_normal(CMD_OFF);
			}
		}
		Serial.print(now.year(), DEC);
		Serial.print('/');
		Serial.print(now.month(), DEC);
		Serial.print('/');
		Serial.print(now.day(), DEC);
		Serial.print(" (");
		Serial.print(days_of_Week[now.dayOfTheWeek()]);
		Serial.print(") ");
		Serial.print(now.hour(), DEC);
		Serial.print(':');
		Serial.print(now.minute(), DEC);
		Serial.print(':');
		Serial.print(now.second(), DEC);
		Serial.println();
	}
}



/*********************************************************************/
void isr_timeout_timer()
{
	portENTER_CRITICAL_ISR(&timeout_timer_mux);
	timeout_interrupt_counter++;
	portEXIT_CRITICAL_ISR(&timeout_timer_mux);
}



/*********************************************************************/
void isr_planning_timer()
{
	portENTER_CRITICAL_ISR(&planning_timer_mux);
	planning_interrupt_counter++;
	portEXIT_CRITICAL_ISR(&planning_timer_mux);
	Serial.println("check planning");
	check_planning();
}



/*********************************************************************/
int check_cmd_null(byte *cmd_vector)
{
	/* TODO try a simpler way */
	byte dest;
	for (int i = 0; i < COMMAND_VECTOR_SIZE; i++)
	{
		command_vector_get(cmd_vector, i, dest)
		if(dest != 0xf){
			return 0;
		}
	}
	return 1;
}



/*********************************************************************/
int select_and_send_cmd()
{
	/* choose the most urgent request */
	current_command = urgent_queue.pop();
	if (!current_command){
		current_command = normal_queue.pop();
		if (!current_command)
			return 0;
	}
	current_command->msg_id = current_msg_id;

	/* creating and sending command using LoRa */
	Serial.println("sending cmd");
	LoRa.beginPacket();
	LoRa.write(current_command->type);
	LoRa.write(current_command->sender);
	LoRa.write(current_command->msg_id);
	LoRa.write(current_command->cmd_vector, COMMAND_VECTOR_SIZE);
	LoRa.endPacket();
	
	/* updating message id variables */
	last_sent_msg_id = current_command->msg_id;
	current_msg_id++;

	Serial.println("Command to send");
	for (int i = 0; i < COMMAND_VECTOR_SIZE; i++)
		Serial.print(current_command->cmd_vector[i], HEX); 

	/* TODO trying to initilize the timer */
	portENTER_CRITICAL(&timeout_timer_mux);
	timeout_interrupt_counter = -1;
	portEXIT_CRITICAL(&timeout_timer_mux);
	return 1;
}



/*********************************************************************/
void listen_and_update()
{
	byte ack_sender, ack_msg_id, ack_nb_rsp, ack_id_relay, ack_cmd, ack_value;

	// try to parse packet
	int packetSize = LoRa.parsePacket();
	if (packetSize){
		//read packet
		while (LoRa.available()){
			byte received_type = LoRa.read();
			Serial.println("received_type : " + String(received_type, HEX));

			switch (received_type){
				case ACK :
					ack_sender = LoRa.read();
					ack_msg_id = LoRa.read();
					ack_nb_rsp = LoRa.read();
					Serial.println("ack_sender : " + String(ack_sender, HEX));
					Serial.println("ack_msg_ID : " + String(ack_msg_id, HEX));
					Serial.println("ack_nb_rsp : " + String(ack_nb_rsp, HEX));

					if (ack_msg_id == last_sent_msg_id){
						for (int i = 0; i < ack_nb_rsp; ++i){
							ack_id_relay = LoRa.read();
							ack_cmd = LoRa.read();
							ack_value = LoRa.read();
							Serial.println("ack_id_relay : " + String(ack_id_relay, HEX));
							Serial.println("ack_cmd : " + String(ack_cmd, HEX));
							Serial.println("ack_value : "  + String(ack_value,HEX));

							/* updating the current command according to the received ACK */
							if (ack_value == ACK_OK){
								Serial.println("changing cmd ..................");
								command_vector_add(current_command->cmd_vector, ack_id_relay, CMD_NULL)
							}
						}
					}
					break;

				case DATA :
					break;

				default :
					break;
			}
		}
	}
}



/*********************************************************************/
void timed_listen()
{
	//Serial.println("Listening" + String(timeout_interrupt_counter));
	while (timeout_interrupt_counter < 1){
		listen_and_update();
	}

	if(timeout_interrupt_counter > 0){
		portENTER_CRITICAL(&timeout_timer_mux);
		timeout_interrupt_counter--;
		portEXIT_CRITICAL(&timeout_timer_mux);

		/*
		total_timeout_interrupt_counter++;
		Serial.print("Interrupt on timeout. Total number: ");
		Serial.println(total_timeout_interrupt_counter);
		*/

		if (!check_cmd_null(current_command->cmd_vector)){
			/* add the current command in the urgent queue */
			urgent_queue.push(current_command);
			
			if(current_command->prog_id == BLE)
				BLE_update(current_command->sender, current_command->msg_id, ACK_ERROR);			
		}else{
			/* send ACK to sub prog */
			switch(current_command->prog_id){
				case 0x0 :
					Serial.println("from planning");
					break;

				case 0x1 :
					Serial.println("from BLE");
					BLE_update(current_command->sender, current_command->msg_id, ACK_OK);
			}
		}
	}
}



/*********************************************************************/
void setup()
{
	pinMode(25,OUTPUT); //Send success, LED will bright 1 second

	Serial.begin(115200);
	//while (!Serial); //Must connect to a computer


	/*------------------------ OLED ------------------------*/  
	SPI.begin(5,19,27,18);

	// Full framebuffer, SW I2C
	U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0,
																							OLED_SCL,
																							OLED_SDA,
																							OLED_RST);
	Display.begin();
	
	//Enable UTF8 support for the Arduino print() function
	Display.enableUTF8Print(); 
	Display.setFont(u8g2_font_ncenB10_tr);


	/*------------------------ LORA ------------------------*/
	LoRa.setPins(SS,RST,DI0);

	if (!LoRa.begin(BAND)) {
		Serial.println("Starting LoRa failed!");
		while (1);
	}
	Serial.println("LoRa Initial OK!");
	

	/*------------------------- CRC -------------------------*/
	LoRa.crc();      


	/*-------------------- TIMEOUT TIMER --------------------*/
	timeout_timer = timerBegin(0, 80, true);
	timerAttachInterrupt(timeout_timer, &isr_timeout_timer, true);
	timerAlarmWrite(timeout_timer, 1000000, true); /* 1000000 = 1s */
	timerAlarmEnable(timeout_timer);


	/*------------------- PLANNING TIMER -------------------*/
	planning_timer = timerBegin(1, 80, true);
	timerAttachInterrupt(planning_timer, &isr_planning_timer, true);
	/* 1000000 = 1s, here 30s, so check the planning twice a minute */
	timerAlarmWrite(planning_timer, 30000000, true);
	timerAlarmEnable(planning_timer);


	/*------------------------- BLE -------------------------*/
	BLE_init();
	BLE_advertise();


	/*-------------------- PLANNING - RTC --------------------*/
	if (! rtc.begin()){
		Serial.println("Couldn't find RTC");
		while (1);
	}

	if (! rtc.isrunning()){
		Serial.println("RTC is NOT running!");
		//set the time according to our PC time
		rtc.adjust(DateTime(__DATE__, __TIME__)); 
	}
}



/*********************************************************************/
void loop()
{
	if (!select_and_send_cmd()){
		//Serial.println("Empty Queues");
	}else{
		timed_listen();
	}
}
