#include <Arduino.h>
#include <string.h>
#include "BLE_Server.h"

BLECharacteristic *BLE_Characteristic[3];
BLEService *BLE_Service[3];
BLEServer *BLE_Server;
boolean BLE_connected = false;

BLEUUID servUUID[] = {
	BLEUUID(SERV_GENERIC_ACCESS),
	BLEUUID("beb5481e-36e1-4688-b7f5-ea07361b26a8")
};

BLEUUID charUUID[] = {
	BLEUUID("beb5483e-36e1-4688-b7f5-ea07361b26a8"),
	BLEUUID("beb5484e-36e1-4688-b7f5-ea07361b26a8"),
	BLEUUID("beb5485e-36e1-4688-b7f5-ea07361b26a8"),
	BLEUUID("beb5486e-36e1-4688-b7f5-ea07361b26a8")
};

class SERV_CONNECTION_CALLBACK : public BLEServerCallbacks {
	void onConnect(BLEServer* pserver) {
		BLE_connected = true;
		Serial.println("onConnect");
	}

	void onDisconnect(BLEServer* pserver) {
		BLE_connected = false;
		Serial.println("onDisconnect");
	}
};



/*==========================================================*/
void add_command(unsigned char *array, int index, unsigned char cmd){
  if(index % 2){ 
	array[index/2] = array[index/2] & 0xf0 ; 
	array[index/2] |= cmd; 
  }else{ 
	array[index/2] = array[index/2] & 0x0f ; 
	array[index/2] |= cmd << 4; 
  }
}
/*==========================================================*/

class CMD_CALLBACK : public BLECharacteristicCallbacks 
{
	virtual void onRead(BLECharacteristic* pCharacteristic)
	{}

	virtual void onWrite(BLECharacteristic* pCharacteristic)
	{
		/*
		Serial.print("data of: ");
		Serial.print(pCharacteristic->getUUID().toString().c_str());
		Serial.print(", value : ");
		Serial.println(pCharacteristic->getValue().c_str());

		for (int i = 0; i < pCharacteristic->getValue().length(); ++i)
			Serial.println((unsigned char)pCharacteristic->getValue().c_str()[i]);
		*/
		command *cmd = new command;
		cmd->prog_id = BLE;
		cmd->type = CMD;
		cmd->sender = BASE_STATION_ID;
		cmd->msg_id = -1;

    /* To command all the satellite at once */
    if(pCharacteristic->getValue().c_str()[1] == TOTAL_NB_PLUGS){
      for (int i = 0; i < TOTAL_NB_PLUGS; i++)
      add_command(cmd->cmd_vector, i, (unsigned char)pCharacteristic->getValue().c_str()[2]);
      normal_queue.push(cmd);
    }else{
      /* for one satellite only */
      for (int i = 0; i < TOTAL_NB_PLUGS; i++)
      add_command(cmd->cmd_vector, i, CMD_NULL);
      add_command(cmd->cmd_vector, 
        (unsigned char)pCharacteristic->getValue().c_str()[1], 
        (unsigned char)pCharacteristic->getValue().c_str()[2]);
      normal_queue.push(cmd);
    }

		//delete [] cmd;
		
		/*
		char *record = new char[4];
		record[0] = ID_BLE;
		record[1] = (char)pCharacteristic->getValue().c_str()[1];
		record[2] = (char)pCharacteristic->getValue().c_str()[2];
		LoRa_toSend.push(record);

		BLE_update(record[1], record[2], 1);
		Serial.println("report is set");
		delete [] record;
		*/
	}
};

class REPORT_CALLBACK : public BLECharacteristicCallbacks 
{
	virtual void onRead(BLECharacteristic* pCharacteristic)
	{
		Serial.println("report read");
		BLE_Characteristic[1]->setValue("Test");
	}

	virtual void onWrite(BLECharacteristic* pCharacteristic)
	{
		Serial.println("report wrote");
	}
};

void BLE_init()
{
  /*Serial.println("MTU lenght :");
  mtu = BLEDevice::getMTU ();
  Serial.println(mtu);
  set=BLEDevice::setMTU (500);
  mtu1 = BLEDevice::getMTU ();
  Serial.println(mtu1);*/

#ifdef SERV_NAME
	BLEDevice::init(SERV_NAME);
#else
	BLEDevice::init("ESP32");
#endif

	BLE_Server = BLEDevice::createServer();
	BLE_Server->setCallbacks(new SERV_CONNECTION_CALLBACK());
	BLE_Service[0] = BLE_Server->createService(SERV_HUMAIN_INTERFACE_DEVICE);

	BLE_Characteristic[0] = BLE_Service[0]->createCharacteristic(
		CHAR_USER_CONTROL_POINT,
		BLECharacteristic::PROPERTY_WRITE);
	
	BLE_Characteristic[0]->setCallbacks(new CMD_CALLBACK());

	BLE_Characteristic[1] = BLE_Service[0]->createCharacteristic(
		CHAR_STRING,
		BLECharacteristic::PROPERTY_READ);

	BLE_Characteristic[1]->setValue("init");

	BLE_Service[0]->start();
}


void BLE_advertise()
{

#ifdef ESP_VERSION_1_02
	BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
	pAdvertising->setScanResponse(true);
	pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
#endif

#ifdef ESP_VERSION_1_00
	BLEAdvertising *pAdvertising = BLE_Server->getAdvertising();
	//Don't set minInterval to 0x06, it will cause advertise issues.
#endif

#ifdef ESP_VERSION_1_02
	BLEDevice::startAdvertising();
#endif
	pAdvertising->addServiceUUID(SERV_HUMAIN_INTERFACE_DEVICE);

#ifdef ESP_VERSION_1_00
	pAdvertising->start();
#endif

	Serial.println("Characteristic defined! Now you can read data in ESP client!");
}

void BLE_update(unsigned char id_src, unsigned char message_id, unsigned char status)
{
	char buff[255];

	switch (status){
		case ACK_OK :
			snprintf(buff, 255, "Command number %d : OK", message_id);
			break;

		case ACK_ERROR :
			snprintf(buff, 255, "Command number %d : KO", message_id);
			break;

		default :
			break;
	}
	
	BLE_Characteristic[1]->setValue(buff);
}
