#include "init_var.h"
#include "lora_types.h"
#include "command_vector.h"


#define NB_RELAY_ON_SATELLITE  2
#define ESP_ID    1
const int relay_id[NB_RELAY_ON_SATELLITE] = {0, 1};
const int pin_of_relay[NB_RELAY_ON_SATELLITE] = {13, 17};


/*
#define NB_RELAY_ON_SATELLITE  1
#define ESP_ID    2
const int relay_id[NB_RELAY_ON_SATELLITE] = {2};
const int pin_of_relay[NB_RELAY_ON_SATELLITE] = {13};
*/

/*
#define NB_RELAY_ON_SATELLITE  1
#define ESP_ID    3
const int relay_id[NB_RELAY_ON_SATELLITE] = {3};
const int pin_of_relay[NB_RELAY_ON_SATELLITE] = {13};
*/

/* TODO byte or int */
byte received_msg_count = -1; //variable for saving incoming message ID  

byte received_type ;
byte received_sender;
byte received_msg_id;
byte buff[TOTAL_NB_RELAYS];

///////Variables for ACK
byte ack_type;
byte ack_sender;
byte ack_msg_id;
byte ack_nb_resp;
byte ack_id_pin_of_relay[TOTAL_NB_RELAYS];
byte ack_cmd[TOTAL_NB_RELAYS];
byte ack_value;


byte command_of_index;

// counter for nb respose
byte counter_resp;


/*********************************************************************/
void setup()
{
	pinMode(25,OUTPUT); //Send success, LED will bright 1 second

	Serial.begin(115200);
	//while (!Serial); //If just the the basic function, must connect to a computer

	SPI.begin(5,19,27,18);
	LoRa.setPins(SS,RST,DI0);	
	U8G2_SSD1306_128X64_NONAME_F_SW_I2C Display(U8G2_R0,  OLED_SCL, OLED_SDA,  OLED_RST); // Full framebuffer, SW I2C

	Display.begin();
	Display.enableUTF8Print();    // enable UTF8 support for the Arduino print() function
	Display.setFont(u8g2_font_ncenB10_tr);

	if (!LoRa.begin(BAND)) {
		Serial.println("Starting LoRa failed!");
		while (1);
	}
	Serial.println("LoRa Initial OK!");
	/////CRC/////
	LoRa.crc();
	/////Encryption///////////////


	/////////////Buffer Init///////////////
	for (int i=0; i<TOTAL_NB_RELAYS; i++){
		buff[i]=0;
	}

	for (int i = 0; i < NB_RELAY_ON_SATELLITE; i++){
		pinMode(pin_of_relay[i], OUTPUT);
		digitalWrite(pin_of_relay[i], LOW); //just for test in setup
	}
}



/*********************************************************************/
void loop()
{
	task_listen(LoRa.parsePacket());
}



/*********************************************************************/
void ack_update (int relay_id, int i)
{
	ack_nb_resp = counter_resp; 
	ack_id_pin_of_relay[i-1] = relay_id; //which plug ????
	ack_cmd[i-1] = buff[relay_id]; 
}



/*********************************************************************/
void send_packet (int nb_cmd) 
{
	int j=0;
	ack_type = ACK; //Plugs -> SB
	ack_sender = ESP_ID;
	ack_msg_id = received_msg_count;
	ack_value = ACK_OK; // OK
		
	//////////////Send Packet///////////////
	Serial.println("---Sending ACK---"); 
	LoRa.beginPacket ();
	LoRa.write (ack_type);
	LoRa.write (ack_sender);
	LoRa.write (ack_msg_id);
	LoRa.write (ack_nb_resp);
	while (j<nb_cmd)
	{
		LoRa.write (ack_id_pin_of_relay[j]); 
		LoRa.write (ack_cmd[j]); /* change this value */
		LoRa.write (ack_value);
		j++;
	}
	LoRa.endPacket ();
	Serial.println("--- ACK sent ---"); 
}



/*********************************************************************/
void task_cmd(int cmd, int plug_index)
{
	switch (cmd){
		case CMD_PING :
			Serial.println ("CMD_PING received");
			delay(relay_id[plug_index]*DELAY);
			counter_resp ++;
			ack_update(relay_id[plug_index], counter_resp);   
			break;
		
		case CMD_ON :
			Serial.println ("CMD_ON received");
			digitalWrite(pin_of_relay[plug_index], LOW); // turn the relay on
			delay(relay_id[plug_index]*DELAY);
			counter_resp ++;
			ack_update(relay_id[plug_index], counter_resp);
			break;

		case CMD_OFF :
			Serial.println ("CMD_OFF received");
			digitalWrite(pin_of_relay[plug_index], HIGH); // turn the relay off
			delay(relay_id[plug_index]*DELAY);
			counter_resp ++;
			ack_update(relay_id[plug_index], counter_resp);
			break;

		case CMD_STATUS :
			Serial.println("Still many things to do...");
			break;

		case CMD_NULL :
			Serial.println("CMD_NULL received, NO ACK");
			break;

		default : 
			Serial.println("Other cases not defined");
			break;
	}
}



/*********************************************************************/
void task_listen(int packetSize)
{
	if (packetSize == 0) return;
	Serial.println("----------------------------------------------------");
	
	/// Initialization of the number of valid responses ///
	counter_resp = 0;
	
	//Read packet from SB
	received_type = LoRa.read();
	received_sender = LoRa.read();
	received_msg_id = LoRa.read();

	/////////////Data Reception///////////////
	Serial.println("Received from: " + String(received_sender, HEX));
	Serial.println("Type: " + String(received_type));
	Serial.println("received_msg_id: " + String(received_msg_id));
	
  
	/* check if sender is allowed to communicate with this plug */
	if (received_sender == BASE_STATION_ID && received_msg_id != received_msg_count){

		/* updating received_msg_count and keep it somewhere */
		received_msg_count = received_msg_id;

		/////////////Buffer Data Reception///////////////
		for (int i=0; i<COMMAND_VECTOR_SIZE; i++)
			buff[i] = LoRa.read();

		/* read each command and do the processing */
		for (int i = 0; i < NB_RELAY_ON_SATELLITE; i++){
			command_vector_get(buff, relay_id[i], command_of_index)
			task_cmd(command_of_index, i);
		}

		/* send the acknowledgement */
		send_packet(counter_resp);
	}
}
