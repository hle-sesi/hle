#include <iostream>

#include "Queue.h"

using namespace hle;
using namespace std;

int main(int argc, char const *argv[])
{
	Queue<char*> q(10);
	
	q.push("hello1");
	q.push("hello2");
	q.push("hello3");
	q.push("hello4");

	char *str = q.peek();
	cout << (void*) str << endl;
	cout << "str[0]:" << str << endl;
	cout << "nb records : " << q.count() << endl;

	char **buff = q.getData();
	for (int i = 0; i < q.count(); ++i)
	{
		cout << "buff[" << i << "] : " << buff[i] << endl;
		cout << "pop of " << i << " : " << q.pop() << endl;
	}

	return 0;
}