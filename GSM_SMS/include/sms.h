#ifndef __SMS_H__
#define __SMS_H__

namespace hle
{

	class sms {
		unsigned long sim_number;
		unsigned long *white_list;
		unsigned long nr_person;
		HardwareSerial serial;

	public:
		/* constructor and destructor */
		sms(HardwareSerial &serial);

		/* SIM800L function */
		int send(const char *mess, unsigned int mess_size);
		int read(char * buff, unsigned buff_size);
		int setMode(unsigned long mode);

		/* local function */
		int add_white_number(unsigned long number);
		int del_white_number(unsigned long number);
		int del_white_number_at(unsigned int number);
		
	}
}


#endif