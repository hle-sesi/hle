# HLE

## Installation

prerequisites for plug control:

 - material
   - ESP32 TTGO
   - male plug connector and female plug connector
   - plug wire : 1.5mm, ground(green, yellow), neutral(while, gray, blue), phase/hot(red, brown) each. Solid wire prefered.
   - relay module
   - electronic wires for arduino.
   - domino or wago to connect wires if there are at least 2 plugs.
 - library
   - [BLE](https://github.com/nkolban/ESP32_BLE_Arduino)
   - [LoRa](https://github.com/sandeepmistry/arduino-LoRa)
   - [RTC](https://github.com/adafruit/RTClib)

**Warning**:
 - power off when making the plug, or changing mountage.
 - be sure the ground wires are connected.
 - make sure that the electronic device do not exceed the relay's max Amps.

### Plug

![schema](schema/plug/plug_mounting.png)


### Code

- `make`
- then open aruino, and upload code to esp32.