#include "command_vector.h"
//#include "init_var.h"



void command_vector_add(unsigned char *array, int index, unsigned char cmd){
	if(index % 2){ 
		array[index/2] = array[index/2] & 0xf0 ; 
		array[index/2] |= cmd; 
	}else{ 
		array[index/2] = array[index/2] & 0x0f ; 
		array[index/2] |= cmd << 4; 
	}
}


void command_vector_get(unsigned char *array, int index, unsigned char dest)
{
	if(index % 2) 
		dest = array[index/2] & 0x0f; 
	else 
		dest = array[index/2] >> 4;
}

/*
// cannot use serial
void command_vector_print(unsigned char *array) 
{
	unsigned char dest;
	Serial.println("Command:");
		//change value 
		for (int i = 0; i < COMMAND_VECTOR_SIZE; i++){
			//command_vector_get(array, i, dest);
			Serial.print(array[i], HEX); 
		}
		Serial.println("");
}
*/