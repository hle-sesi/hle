#ifndef _LORA_TYPES_H_
#define _LORA_TYPES_H_

/* 
 * Message types 
 * 
 * CMD  : send by base station to plugs
 * ACK  : from plugs to base station
 * DATA : sensors to base station 
 * 
 */

#define CMD     0x01
#define ACK     0x02
#define DATA    0x03



/* 
 * Command types, sub-type from CMD type 
 * 
 * CMD_PING     : reply is an ACK_OK type 
 * CMD_ON       : reply is an ACK_OK or ACK_ERROR type 
 * CMD_OFF      : reply is an ACK_OK or ACK_ERROR type 
 * CMD_STATUS   : reply is an ACK_ON or ACK_OFF type 
 * CMD_NULL     : there is no reply
 * 
 */

#define CMD_PING    0x0
#define CMD_ON      0x1
#define CMD_OFF     0x2
#define CMD_STATUS  0x3
#define CMD_NULL    0xf



/* 
 * ACK types 
 * 
 * Meaning of each acknowledgement
 * ACK_OK       : no problem
 * ACK_ERROR    : problem, the problem is specified in the 4 LSB
 * ACK_ON       : pin state is high level, pin value is in the LSB 
 * ACK_OFF      : pin state is low level, pin value is in the LSB 
 * 
 */

#define ACK_OK      0x00
#define ACK_ERROR   0x10
#define ACK_ON      0x02
#define ACK_OFF     0x03



/*
 * plug_id
 */
#define PLANNING 	0x0
#define BLE				0x1


#endif /* _LORA_TYPES_H_ */