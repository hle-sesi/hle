#ifndef _COMMAND_VECTOR_H_
#define _COMMAND_VECTOR_H_


//#include "init_var.h"


/*
 * These functions will be used to have 4 bits commands
 */

//void command_vector_add(unsigned char *array, int index, unsigned char cmd);


//void command_vector_get(unsigned char *array, int index, unsigned char dest);


//void command_vector_print(unsigned char *array);


/*
 * Create and initilize a command vector
 * the size is defined by COMMAND_VECTOR_SIZE
 */
/*
#define command_vector_init(name) \
		byte name [COMMAND_VECTOR_SIZE]; \
		for (int i = 0; i < COMMAND_VECTOR_SIZE; i++) \
			name[i] = 0xf;
*/

/*
 * Add the command `cmd` into the command vector `array` 
 * at index `index`
 */

#define command_vector_add(array, index, cmd) \
				{if(index % 2){ \
					array[index/2] = array[index/2] & 0xf0 ; \
					array[index/2] |= cmd; \
				}else{ \
					array[index/2] = array[index/2] & 0x0f ; \
					array[index/2] |= cmd << 4; \
				}}


/*
 * Get the command value at index `index`
 * from the command vector `array`
 * and write the result at `dest`
 */

#define command_vector_get(array, index, dest) \
				{if(index % 2) \
					dest = array[index/2] & 0x0f; \
				else \
					dest = array[index/2] >> 4;}


/*
#define command_vector_check(array, index, value) \
		byte dest; \
		command_vector_get(array, index, dest) \
		dest == value;

*/
		
/* 
 * Print the command vector `array`
 * BE CAREFUL: need to be adapted for arduino IDE 
 */
/*
#define command_vector_print(array) \
		for (i = 0; i < COMMAND_VECTOR_SIZE; i++){ \
			byte dest; \
			command_vector_get(array, i, dest) \
			Serial.print("0x%x \t", dest); \
		}
		Serial.println("");
*/


#endif /* _COMMAND_VECTOR_H_ */