#ifndef _BASE_STATION_H_
#define _BASE_STATION_H_


#include "init_var.h"

/* prog_id see lora_types.h */
typedef struct command
{
	byte prog_id;
	byte type;
	byte sender;
	byte msg_id;
	byte cmd_vector[COMMAND_VECTOR_SIZE];
}command;

/* push a command with `cmd_value_for_all` for all the satellites */
void push_cmd_to_normal(unsigned char cmd_value_for_all);

/* check the hour and to something according to the planning */
void check_planning();

/* isr for the timeout interrupt */
void isr_timeout_timer();

/* isr for the planning interrupt */
void isr_planning_timer();

/* check if the `cmd_vector is null` */
int check_cmd_null(byte *cmd_vector);

/* 
 * select the command to send
 * return 1 and a valid command or 
 * return 0 if the queues are empty 
 */ 
int select_and_send_cmd();

/* listen the ack of a command and update the command */
void listen_and_update();

/* listen_and_update with timeout */
void timed_listen();



#endif /* _BASE_STATION_H_ */