#ifndef _INIT_VAR_H_
#define _INIT_VAR_H_

#include <SPI.h>
#include <LoRa.h>
#include <Arduino.h>
#include <U8g2lib.h>   // https://github.com/olikraus/U8g2_Arduino
#include <Wire.h>  // for I2C with RTC module

#include <RTClib.h>       //to show time -- https://github.com/adafruit/RTClib
//#include "command_vector.h"
//#include "Queue.h"

// GPIO5  -- SX1278's SCK
// GPIO19 -- SX1278's MISO
// GPIO27 -- SX1278's MOSI
// GPIO18 -- SX1278's CS
// GPIO14 -- SX1278's RESET
// GPIO26 -- SX1278's IRQ(Interrupt Request)


#define SS      18
#define RST     14
#define DI0     26
#define BAND    433E6


// I2C OLED Display works with SSD1306 driver
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16



#define TOTAL_NB_RELAYS 4
#define COMMAND_VECTOR_SIZE (TOTAL_NB_RELAYS/2)



#define BASE_STATION_ID 0x00

#define DELAY       100


/* Planning variables */
#define WEEKEND_OPEN_HOUR   9
#define WEEKEND_OPEN_MIN    0
#define WEEKEND_CLOSE_HOUR  1
#define WEEKEND_CLOSE_MIN   0

#define WEEKDAYS_OPEN_HOUR  18
#define WEEKDAYS_OPEN_MIN   26
#define WEEKDAYS_CLOSE_HOUR 0
#define WEEKDAYS_CLOSE_MIN  0


#endif /* _INIT_VAR_H_ */